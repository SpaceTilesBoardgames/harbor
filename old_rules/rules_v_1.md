Harbor
=======

**Number of players :** 3 to 5  
**Number of cards :** 52  
**Duration :** should last 30 to 45 minutes.  
**Summary :** In Harbor, each player bargains, trades, sells and builds to make the most profit out of the different goods that arrive in the harbor. In turn, each one plays the Harbor Administrator and decides who gets which good or enhancement.

Using a standard Deck:
-------

A standard 52 cards deck can be used to play the game but additionnal tokens will be neccessary to represent gold. In the final PnP version, tokens won't be necessary and gold counting will be possible with two cards and what will be printed on their back.

Card color stands for the type of good it is. ( e.g.: Hearts may stand for copper and Spades for wheat, definitive goods aren't yet decided )
Card value stands for which enhancement it is. (Ace,2,3,4,5,6 stands for Storage enhancements. 7 to 10 stands for Docks enhancements. J, Q, K stands for Merchant enhancement)

Rules
-------

### Preparation
Discard as many cards as the number of players. The rest of the cards are the draw pile. The last player who went to an Harbor will be the first Harbor Administrator. The harbor Administrator starts with 4 golds, the player to his/her left 5 golds and so on. Each player starts with a storage space of 2.

### Round
Each round is divided in two phases. The Bargain phase and the Sale & Enhancement phase. At the end of the round the player to the left of the Harbor Administrator becomes the Harbor Administrator and a new round begins.

### End and win conditions
If during the Bargain phase there's not enough cards to draw or the last card is drawn, the game ends at the end of the round. The richest player wins the game.

### Bargain phase
The Harbor Administrator draws as many cards as the number of players minus one. These cards are placed in the middle of the table for all to see. The Harbor Administrator then decides who gets which card but other players may offer to give gold or trade other cards they have in storage in exchange. He/She can get or give any number of card. The only restriction is that players never exceed their storage capacity. If the Harbor Administrator can't dispose of all the cards in the middle (in case other players don't want them and he/she hasn't enough storage to take them himself/herself for instance) He/She must pay a fee ( As many gold as the number of players by card leftover).

### Sale & Enhancement phase
Each player (starting by the one at the left of the Harbor Administrator and finishing by the Harbor Administrator) may choose between selling a good or building an enhancement.  
**Selling :** A good is sold for as many gold as there is cards of this type of good in the storages of every player (including the card being sold). A player may only sell one card during this phase.  
**Enhancement :** A player can instead choose to enhance his company. Building enhancement requires several card of the same range of value but not necessary the same color or value ( e.g. : to build a first storage a player may use a 2 of hearts and a 3 of spades). To build an enhancement the player simply keeps one the card, flips it 90° in front of him and discards the others. A player may only build one enhancement during this phase.

### Enhancements
**Storage :** Each one allows players to store one additional card.
1st storage requires 2 cards, 2nd, 3 cards, 3rd, 4 cards and so on.  
**Docks :** Each one allows players to draw one more card during the bargain phase when they're the Harbor Administrator.
1st Dock requires 3 cards, 2nd, 4 cards and so on.  
**Merchant :** When building a merchant enhancement, the type of good on the card kept means it is a merchant of this type of good. A merchant allows a player to sell as many cards - of the same type of good - as He/She wants during the Sale & Enhancement phase. Cards are sold with a bonus of +2 gold and they do not count as sale & enhancement action. This means the player can also sell one card of another type or build an enhancement. If a player has several merchants of the same type, gold bonuses stack.
First merchant requires 2 cards, second merchant, 4 cards and so on.

### Other
Players may exchange gold any time they want.
Cards flipped 90° represent enhancements and do not count as card in storage.
Cards in storage are placed in front of players for all the other players to see. 